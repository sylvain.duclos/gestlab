﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestLab.Models
{
    public class Equipement
    {
       [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string Designation { get; set; }
        public string DateCreation { get; set; }
        public string ReferenceEquipement { get; set; }
    }
}
