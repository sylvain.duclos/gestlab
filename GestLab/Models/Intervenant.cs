﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestLab.Models
{
    public class Intervenant
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
	    public string Raison_Sociale { get; set; }

    }
}
