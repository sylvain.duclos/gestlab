﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestLab.Models
{
    public class Cause
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string CauseIntervention { get; set; }

    }
}
