﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace GestLab
{
    public class Personne
    {

            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int ID { get; set; }
            public string Nom { get; set; }
            public string Prenom { get; set; }
            public string Mail { get; set; }
            public string Telephone { get; set; }

    }
}
